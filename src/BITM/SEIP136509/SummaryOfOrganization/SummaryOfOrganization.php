<?php
namespace App\SummaryOfOrganization;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
class SummaryOforganization extends DB{
    public $id;
    public $org_name;
    public $org_summary;

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("org_name",$postVariable)){
            $this->org_name=$postVariable['org_name'];
        }
        if(array_key_exists("org_summary",$postVariable)){
            $this->org_summary=$postVariable['org_summary'];
        }
    }

    public function store(){
        $arrData=array($this->org_name,$this->org_summary);
        $sql="insert into summary_of_organization(org_name,org_summary)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted Successfully");
        }
        else{
            Message::message("Falied!Data has not been inserted Successfully");
        }

        Utility::redirect('create.php');
    }
}
?>
